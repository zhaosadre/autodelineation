import pydicom
import numpy as np
from PIL import Image, ImageDraw

def RT2mask(rtStruct, ROIName, GridSize, ImagePositionPatient, PixelSpacing):
  for i, dcm_struct in enumerate(rtStruct["StructureSetROISequence"]):
    if (not (dcm_struct["ROIName"]==ROIName)):
      continue
    
    # TODO use ROINumber in case ordering is not the same for StructureSetROISequence and ROIContourSequence
    dcm_contour = rtStruct["ROIContourSequence"][i]
    
    mask3d = np.zeros(GridSize, dtype=np.uint8)
    
    for dcm_slice in dcm_contour["ContourSequence"]:
      Slice = {}
      
      if (len(dcm_slice["ContourData"])<3):
        continue
        
      # list of Dicom coordinates
      Slice["Z_dcm"] = float(dcm_slice["ContourData"][2])
      
      # list of coordinates in the image frame
      Slice["XY_img"] = list(zip( 
        ((np.array(dcm_slice["ContourData"][1::3]) - ImagePositionPatient[1]) / PixelSpacing[1]),
        ((np.array(dcm_slice["ContourData"][0::3]) - ImagePositionPatient[0]) / PixelSpacing[0])
      ))
      Slice["Z_img"] = -(Slice["Z_dcm"] - ImagePositionPatient[2]) / PixelSpacing[2]
      
      Slice["Slice_id"] = int(round(Slice["Z_img"]))
      
      if (len(Slice["XY_img"])<2):
        continue
      
      # convert polygon to mask (based on PIL - fast)
      img = Image.new('L', (GridSize[0], GridSize[1]), 0)
      ImageDraw.Draw(img).polygon(Slice["XY_img"], outline=1, fill=1)
      mask = np.array(img)
      
      mask3d[:, :, Slice["Slice_id"]] = np.logical_or(mask3d[:, :, Slice["Slice_id"]], mask)

    return mask3d

  return None
  
