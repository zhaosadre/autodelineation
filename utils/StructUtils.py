
import os
from scipy.ndimage import zoom
from bson.json_util import dumps
import numpy as np
import copy
import pydicom
import numpy as np
from PIL import Image, ImageDraw

class StructUtils:
  def __init__(self):
    self.data = []
    self.meta = []
  
  def appendDicomData(self, metaData):      
    self.meta.append(metaData)
  
  def adaptToSeries(self, seriesMeta):
    for i, metaItem in enumerate(self.meta):
      meta = copy.deepcopy(metaItem)
      
      if isinstance(metaItem, dict) and not isinstance(metaItem["Rows"], pydicom.DataElement):
        ImagePositionPatient = list(map(float, seriesMeta[0]["ImagePositionPatient"]))
        PixelSpacing = list(map(float, seriesMeta[0]["PixelSpacing"]))
        SliceThickness = seriesMeta[0]["SliceThickness"].value
        Rows = seriesMeta[0]["Rows"].value
        Columns = seriesMeta[0]["Columns"].value
        Slices = len(seriesMeta)
        ImageOrientationPatient = list(map(float, seriesMeta[0]["ImageOrientationPatient"]))
        StudyDate = str(meta["StudyDate"])
        SeriesDate = str(meta["SeriesDate"])
        Manufacturer = str(meta["Manufacturer"])
        PatientID = str(meta["PatientID"])
        StudyInstanceUID = str(meta["StudyInstanceUID"])
        SeriesInstanceUID = str(meta["SeriesInstanceUID"])
        
        xMin = ImagePositionPatient[0]
        xMax = ImagePositionPatient[0]+Rows*PixelSpacing[0]
        yMin = ImagePositionPatient[1]
        yMax = ImagePositionPatient[1]+Columns*PixelSpacing[1]
        zMin = ImagePositionPatient[2]+Slices*SliceThickness
        
        ROIContourSequence = []
        StructureSetROISequence = []
        for k, contour in enumerate(metaItem["ROIContourSequence"]):
          StructureSetROISequence.append( {
            "ROINumber": str(metaItem["StructureSetROISequence"][k]["ROINumber"]),
            "ROIName": str(metaItem["StructureSetROISequence"][k]["ROIName"])
          })
          
          contourSequence = []
          for j, contourItem in enumerate(contour["ContourSequence"]):
            if j<len(contour["ContourSequence"])-1:
              nextContourZ = contour["ContourSequence"][j+1]["ContourData"][2]
            else:
              nextContourZ = contour["ContourSequence"][j]["ContourData"][2]
            
            xs = contourItem["ContourData"][0::3]
            ys = contourItem["ContourData"][1::3]
            zs = contourItem["ContourData"][2::3]
            z = float(zs[0])
            
            newZ1 = zMin+round((z-zMin)/SliceThickness)*SliceThickness
            newZ2 = zMin+round((nextContourZ-zMin)/SliceThickness)*SliceThickness
            
            for z in np.arange(newZ2-SliceThickness, newZ1-SliceThickness, -SliceThickness):
              if z<seriesMeta[-1].SliceLocation or z>seriesMeta[0].SliceLocation:
                continue
              
              xs = list(map(lambda x: xMin if x<xMin else x, xs))
              xs = list(map(lambda x: xMax if x>xMax else x, xs))
              ys = list(map(lambda x: yMin if x<yMin else x, ys))
              ys = list(map(lambda x: yMax if x>yMax else x, ys))
              zs = list(map(lambda x: zMin+round((z-zMin)/SliceThickness)*SliceThickness, zs))
              
              contourItem["ContourData"][0::3] = xs
              contourItem["ContourData"][1::3] = ys
              contourItem["ContourData"][2::3] = zs
              
              contourSequenceItem = {
                "NumberOfContourPoints": contourItem["NumberOfContourPoints"],
                "ContourData": list(map(float, copy.deepcopy(contourItem["ContourData"])))
              }
            
              contourSequence.append(contourSequenceItem)
          
          roiConstourSequenceItem = {
            "ROIDisplayColor": list(map(float, contour["ROIDisplayColor"])),
            "ContourSequence": contourSequence
          }
          
          ROIContourSequence.append(roiConstourSequenceItem)
      else:
        print("TODO")   
      
      self.meta[i] = {
        "modalities": str("RTSTRUCT"),
        "modality": str("RTSTRUCT"),
        "Modality": str("RTSTRUCT"),
        "StudyDate": StudyDate,
        "SeriesDate": SeriesDate,
        "Manufacturer": Manufacturer,
        "PatientName": "",
        "PatientID": PatientID,
        "BodyPartExamined": "",
        "StudyInstanceUID": StudyInstanceUID,
        "SeriesInstanceUID": SeriesInstanceUID,
        "StructureSetROISequence" : StructureSetROISequence,
        "ROIContourSequence": ROIContourSequence,
        "SeriesDescription": "",
        "Rows": Rows, #This is non standard
        "Columns": Columns, #This is non standard
        "Slices": Slices, #This is non standard
        "SliceThickness": SliceThickness, #This is non standard
        "PixelSpacing": PixelSpacing, #This is non standard
        "ImagePositionPatient": ImagePositionPatient, #This is non standard
        "ImageOrientationPatient": ImageOrientationPatient #This is non standard
      }
  
  def getMeta(self):
    return self.meta
  
