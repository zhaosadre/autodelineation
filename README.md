<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<div align="center">
  <h1>Autodelineation tools</h1>
  <p>This project contains deep learning prediction tools for organ segmentation in radiation therapy</p>
</div>

<hr />

## Developing
TODO

### Requirements
TODO

### Getting Started
TODO

## Contributors
[Pilab](https://pilab.be/) and [MIRO](https://uclouvain.be/fr/instituts-recherche/irec/miro): [Eliott Brion](https://github.com/eliottbrion/), Jean Leger, Steven Michiels, Ana Maria Barragan Montero, [Sylvain Deffet](https://gitlab.com/deffets)

## Acknowledgments

## License

APACHE 2.0

